**AgriMart Platform**

**Introduction**

Build a common web platform that can assist corporates purchase the
products online directly from the farmers, where we'll have several
categories like Fruits, Vegetables, Food Grains, Spices and so on, each
subdivided into more categories like Fruits consisting of Apples,
Grapes, Banana, Papaya and so on; Vegetables containing Potato,
Drumsticks, Ginger, etc.

Corporates should be able to log in to web platform and get a customized
homepage that contains farm products ordered by their category
preferences. Farmers upload their products with their details like
quantity, price and a shelf life of a product (time duration to sell
their products) and buyers view these details and order that product.

Admin should be able to approve the farmer login and manage users and
have a look at pre-orders and notify the farmers also assures the
quality check, payment and delivery status.

**Basic operations**

The web platform should be browsable without anyone logged in:
products/categories have a default order in which they are shown.

The platform should have a way to register and login for farmers to
upload product images and product details, for corporates to buy the
products. Administrators should also have the ability to delete fake
records that have been uploaded by anyone. Farmer can delete the
products on their own and corporates get more discount on products based
on the shelf life.
