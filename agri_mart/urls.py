from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from django.conf import settings

urlpatterns = [
    path('home', views.home, name='home'),
    path('fhome', views.fhome, name='fhome'),
    path('bhome', views.bhome, name='bhome'),
    path('login', views.Login, name='login'),
    path('register', views.register_page, name='register'),
    path('products/<int:id>', views.viewproducts, name = 'products'),
    path('fproductdetails/<int:id>',views.farmerproductdetails, name ='fproductdetails'),
    path('delete/<int:id>', views.delete, name ='delete'),
    path('preorder/<int:id>',views.preorder, name = 'preorder'),
    path('buyproduct/<int:id>', views.buyproduct,name="buyproduct"),
    path('addtocart/<int:id>', views.addtocart, name='addtocart'),
    path('increment/<int:id>', views.increment,name='increment'),
    path('decrement/<int:id>', views.decrement,name='decrement'),
    path('logout', views.Logout, name='logout'),
    path('search', views.search_prod, name='search'),
    path('notificationview/<int:id>', views.viewnotification, name = 'notificationview'),
    path('delnotification/<int:id>', views.delnotification, name = 'delnotification'),
    path('acceptorders',views.acceptorders, name = 'acceptorders'),
    path('profile',views.farmerproducts,name = 'profile'),
    path('cart', views.cart, name = 'cart'),
    path('checkout', views.checkout, name = 'checkout'),
]
