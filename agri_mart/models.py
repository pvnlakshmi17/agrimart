from django.db import models
from django.contrib.auth.models import User, Group
import django.utils.timezone


# Create your models here.
class Admin_cat(models.Model):
    cat_name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.cat_name

class Admin_product(models.Model):
    cat_name = models.ForeignKey(Admin_cat, on_delete=models.CASCADE, default='')
    prod_name = models.CharField(max_length=100)
    prod_image = models.ImageField(upload_to='prod_images')

    def __str__(self):
        return f'{self.cat_name},{self.prod_name},{self.id},{self.cat_name.id}'
    


class FarmerDetails(models.Model):
    username = models.CharField(max_length=100)
    #email =models.EmailField(max_length=100)
    password=models.CharField(max_length=100)
    phone_number =models.IntegerField()
    cust_info = models.CharField(max_length = 100,default = 'seller')
        
    def __str__(self):
        return self.username

class BuyerDetails(models.Model):
    username = models.CharField(max_length=100)
    #email =models.EmailField(max_length=100)
    password=models.CharField(max_length=100)
    phone_number =models.IntegerField()
    cust_info = models.CharField(max_length = 100, default = 'buyer')

    
    def __str__(self):
        return self.username

class Product_details(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = '')
    cat = models.ForeignKey(Admin_cat, on_delete=models.CASCADE)
    pro = models.ForeignKey(Admin_product, on_delete=models.CASCADE)
    quantity_available = models.PositiveIntegerField(default= 0 )
    due_time = models.DateTimeField(default= django.utils.timezone.now)
    shelf_time = models.PositiveIntegerField(default=0)
    price = models.PositiveIntegerField(default= 0 )
    discount = models.PositiveIntegerField(default= 0 )
    soil = models.CharField(max_length = 100, default = 'Loam Soil')


    def __str__(self):
        return f'{self.pro.id},{self.pro.prod_name}'

class FarmerProducts(models.Model):
    farmerdetails = models.ForeignKey(FarmerDetails, on_delete=models.CASCADE)
    farmerproductname = models.ForeignKey(Admin_product,on_delete=models.CASCADE)

class PreOrders(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = '')
    pre_prod = models.ForeignKey(Admin_product, on_delete= models.CASCADE)
    quantity_required = models.PositiveIntegerField(default=0, null= True, blank= True)
    sche_time = models.DateTimeField()
    advance = models.PositiveIntegerField(default = 0)

    def __str__(self):
        return f'{self.id},{self.user.username}, {self.pre_prod.prod_name}'
    



class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = '')
    item = models.ForeignKey(Product_details, on_delete = models.CASCADE )
    quantity_required = models.PositiveIntegerField(default=0)
    discount = models.PositiveIntegerField(default= 0)
    finalprice = models.PositiveIntegerField(default=0)
    subtotal = models.PositiveIntegerField(default = 0)

class Payment_status(models.Model):
    cart_stat = models.ForeignKey(Cart, on_delete= models.CASCADE )
    Payment_status = models.CharField(max_length=100)
