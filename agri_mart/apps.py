from django.apps import AppConfig


class AgriMartConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'agri_mart'
