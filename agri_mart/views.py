from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from agri_mart.models import *
import datetime
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required


# Create your views here.
def home(request):
    cat = Admin_cat.objects.all()
    custompre = PreOrders.objects.all()
    return render(request,'agri_mart/home.html',{'category':cat,'preorder':custompre})

def fhome(request):
    cat = Admin_cat.objects.all()
    custompre = PreOrders.objects.all()
    return render(request,'agri_mart/fhome.html',{'category':cat,'preorder':custompre})

def bhome(request):
    cat = Admin_cat.objects.all()
    custompre = PreOrders.objects.all()
    return render(request,'agri_mart/bhome.html',{'category':cat,'preorder':custompre})

def register_page(request):
    if request.method =="POST":
        username=request.POST["username"]
        phone_number =request.POST["phonenumber"]
        password =request.POST["password"]
        cnf_password =request.POST["confirm_password"]
        cust_info = request.POST["cust_info"]
        myuser =User.objects.create_user(username = username, email= None, password = password)
        myuser.save()
        g1 = Group.objects.get(name='Farmers')
        g2 = Group.objects.get(name='Buyers')
        if cust_info.lower() == "buyer":
            buyerDetails = BuyerDetails(username = username, password = password, phone_number = phone_number)
            buyerDetails.save()
            g2.user_set.add(myuser)
        elif cust_info.lower() == "seller":
            farmerDetails = FarmerDetails(username = username, password = password, phone_number = phone_number)
            farmerDetails.save()
            g1.user_set.add(myuser)
        return redirect("home")
        context={}

    return render(request,'agri_mart/register.html')

@login_required(login_url='login')
def farmerproducts(request):
    farmerproducts = Product_details.objects.filter(user = request.user)
    preorders = PreOrders.objects.all()
    user = request.user
    return render(request,'agri_mart/fprofile.html', {'ownproducts' : farmerproducts, 'user': user, 'preorders' :preorders})

@login_required(login_url='login')
def cart(request):
    cart = Cart.objects.filter(user = request.user)
    user = request.user
    return render(request, 'agri_mart/cart.html', {'cart': cart})

def Login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        farmers = list(FarmerDetails.objects.all().values_list('username'))
        buyers = list(BuyerDetails.objects.all().values_list('username'))
        if user is not None:
            login(request, user)
            if (str(username),) in farmers :
                return redirect("fhome")
            elif (str(username),) in buyers :
                return redirect("bhome")
        else:
            messages.error(request,'username or password not correct')
            return render(request, 'agri_mart/login.html')
    else:
        return render(request, 'agri_mart/login.html')
    

def Logout(request):
    logout(request)
    return redirect("home")




def viewproducts(request, id):
    cat_name = Admin_cat.objects.get(id = id)
    products = Admin_product.objects.filter(cat_name = cat_name)
    lst = list(products)
    return render(request,'agri_mart/products.html', {'list':lst})

@permission_required('agri_mart.add_product_details', login_url='login')
def farmerproductdetails(request,id):
    if request.method =="GET":
        products = Admin_product.objects.get(id =id)
        return render(request, 'agri_mart/farmerproductdetails.html',{'products' : products})
    elif request.method =="POST":
        products = Admin_product.objects.get(id=id)
        cat = Admin_cat.objects.get(id = products.cat_name.id)
        quantity=request.POST["quantity"]
        due_time =request.POST["shelftime"]
        price =request.POST["price"]
        discount =request.POST["discount"]
        soil = request.POST['soiltype']
        start_date = datetime.datetime.today()
        end_date = datetime.datetime.strptime(request.POST.get('shelftime'), "%Y-%m-%d")
        shelftime = abs((end_date-start_date).days)
        pd = Product_details(user = request.user,pro = products, cat = cat, quantity_available = quantity,due_time = due_time, shelf_time = shelftime, price = price, discount = discount, soil = soil)
        pd.save()
        product = Product_details.objects.filter(user = request.user)
        return render(request,'agri_mart/successadding.html',{'productsadded': product})
    
@login_required(login_url='login')
def delete(request, id):
    item = Product_details.objects.filter(pk = id)
    item.delete()
    product = Product_details.objects.filter(user = request.user)
    return render(request,'agri_mart/successadding.html',{'productsadded': product})

@permission_required('agri_mart.add_preorders', login_url='login')
def preorder(request, id):
        if request.method == 'GET':
            preprod = Admin_product.objects.filter(id=id)
            return render(request,'agri_mart/preorder.html',{'order': preprod})
        if request.method == 'POST':
            preprod = Admin_product.objects.get(id=id)
            prename = preprod.prod_name
            quantity = request.POST.get('quantity',0)
            advance = request.POST.get('advance',0)
            duedate = request.POST.get('duedate',datetime.date.today())
            preorder = PreOrders(user = request.user,pre_prod = preprod,quantity_required = quantity, sche_time = duedate,advance = advance )
            preorder.save()
            custompre = PreOrders.objects.filter(user = request.user)
            print(custompre)
            return render(request,'agri_mart/notification.html',{'preorder':custompre})
        
@permission_required('agri_mart.add_cart', login_url='login')
def buyproduct(request,id):
    products = Product_details.objects.filter(pro_id = id)
    for each in products :
        if each.shelf_time < 10 :
            each.discount += 20
            each.save()
    return render(request,'agri_mart/buyerpage.html',{'products': products})

@permission_required('agri_mart.add_cart', login_url='login')
def addtocart(request, id):
    cartelements = Product_details.objects.get(id = id)
    cart, created = Cart.objects.get_or_create(item = cartelements, user =request.user)
    cart.quantity_required = cart.quantity_required + 1
    cartelements.quantity_available -= 1
    cartelements.save()
    cart.subtotal = (cart.quantity_required * cart.item.price) * ((100 - cart.item.discount)/100)
    cart.save()
    cartele = Cart.objects.filter(user = request.user)
    finalprice = sum([each.subtotal for each in cartele])
    return render(request,'agri_mart/cart.html',{'cartelements' : cartelements, 'cart': cartele, 'finalprice' : finalprice})

def increment(request,id):
    quantity = Cart.objects.get(id = id)
    quantity.quantity_required += 1
    quantity.item.quantity_available -=1
    quantity.subtotal = (quantity.quantity_required * quantity.item.price ) * ((100 - quantity.item.discount)/100)
    quantity.save()
    cartele = Cart.objects.filter(user = request.user)
    finalprice = sum([each.subtotal for each in cartele])
    return render(request,'agri_mart/cart.html',{'cart': cartele, 'finalprice' : finalprice})

def decrement(request,id):
    quantity = Cart.objects.get(id = id)
    if quantity.quantity_required > 1:
        quantity.quantity_required -= 1
        quantity.item.quantity_available +=1
        quantity.subtotal = (quantity.quantity_required * quantity.item.price) *((100 - quantity.item.discount)/100)
        quantity.save()
        cartele = Cart.objects.filter(user = request.user)
        finalprice = sum([each.subtotal for each in cartele])
        return render(request,'agri_mart/cart.html', {'cart': cartele, 'finalprice' : finalprice})
    
    elif quantity.quantity_required == 1 : 
        quantity.delete()
        cartele = Cart.objects.filter(user = request.user)
        finalprice = sum([each.subtotal for each in cartele])
        return render(request,'agri_mart/cart.html', {'cart': cartele, 'finalprice' : finalprice})

def search_prod(request):
    if request.method == "POST":
        searched = request.POST['searched']
        product = Admin_product.objects.filter(prod_name__icontains = searched)
        # products = Product_details.objects.filter(pro=searched)
        return render(request, 'agri_mart/search.html', {'searched':searched, 'products':product})
    else:
        return render(request, 'agri_mart/search.html')
    

@login_required(login_url='login')    
def viewnotification(request,id):
    if request.method == 'GET':
        notification = PreOrders.objects.filter(id = id)
        return render(request, 'agri_mart/notificationview.html', {'notification': notification})

@login_required(login_url='login')
def acceptorders(request):
    return render(request,'agri_mart/acceptorder.html')

@login_required(login_url='login')
def delnotification(request,id):
    preorders = PreOrders.objects.get(id=id)
    print(preorders)
    preorders.delete()
    return render(request,'agri_mart/fprofile.html',{'acceptorders': preorder})

def checkout(request):
    if request.method == 'POST':
        return render(request, 'agri_mart/order.html')
    return render(request, 'agri_mart/checkout.html')









    






