# Generated by Django 3.2.12 on 2023-03-06 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agri_mart', '0005_auto_20230306_0805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product_details',
            name='pro_img',
            field=models.ImageField(upload_to='prod_images'),
        ),
    ]
